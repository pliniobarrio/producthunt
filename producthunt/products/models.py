from django.db import models
from django.contrib.auth.models import User

class Product(models.Model):
    Title = models.CharField(max_length=200)
    pub_date = models.DateTimeField(auto_now=False)
    body = models.TextField()
    url = models.TextField()
    image = models.ImageField(upload_to='images/')
    icon = models.ImageField(upload_to='images/')
    votes_total = models.IntegerField(default=1)
    hunter = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.Title

    def summary(self):
        if len(self.body) > 100:
            result = self.body[:100] + '...'
        else:
            result = self.body
        return result

    def pub_date_pretty(self):
        return self.pub_date.strftime('%B, %Y')
